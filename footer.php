<div>
  <img src="<?php echo get_template_directory_uri(); ?>/img/footer-invest.svg">
  <p>Town of Queen Creek, Economic Development  |  <a href="https://www.google.com/maps/place/22358+S+Ellsworth+Rd,+Queen+Creek,+AZ+85142/@33.2447338,-111.6355871,17z/data=!3m1!4b1!4m2!3m1!1s0x872a4d1679b3039b:0xda284f99524cc24a" target="_blank">22358 S Ellsworth Road Queen Creek, AZ  85142</a>  |  TELEPHONE  480 358 3523  |  Email  <a href="mailto:investtheqc@queencreek.org">investtheqc@queencreek.org</a></p>
  <p class="ksstag">Site Design and Development by <a href="http://kitchensinkstudios.com" target="_blank">Kitchen Sink Studios</a></p>
</div>
