<?php include('head.php') ?>
<title>Town of Queen Creek :: Town Center Visioning Project</title>
</head>
    <body ng-controller="ProjectController as projects" <?php body_class( $class ); ?>>
        <!--[if lt IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <span ng-controller="TabController as inttab">
      <?php include('header.php') ?>
      <?php include('map.php') ?>
      <?php include('location.php') ?>
    </span>
    <?php include('scripts.php') ?>
    </body>
</html>
