<header>
  
  <a href="http://www.queencreek.org/" target="_blank">
  <div class="logo">
    <img src="<?php echo get_template_directory_uri(); ?>/img/queen-creek-logo.svg">
  </div>
  </a>

  <div class="title">
    <h1>Town of Queen Creek</h1>
    <h2>Town Center Visioning Project</h2>
  </div>
  <div class="social">
    <ul>
      <a href="https://twitter.com/TOQC_official" target="_blank"><li class="twitter"></li></a>
      <a href="https://www.facebook.com/queencreek" target="_blank"><li class="facebook"></li></a>
    </ul>
  </div>
</header>