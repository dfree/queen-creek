<?php

// ADD SVG UPLOAD SUPPORT
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
// add the file extension to the array
    $existing_mimes['svg'] = 'mime/type';
// call the modified list of extensions
    return $existing_mimes;
}


function remove_menu(){
    $admins = array( 
        'kssdev', 
        'jfishkss'
    );
    $current_user = wp_get_current_user();
    if( !in_array( $current_user->user_login, $admins ) ){
        remove_menu_page('edit.php?post_type=acf-field-group');
        remove_menu_page( 'themes.php' );      
        remove_menu_page( 'plugins.php' );  
        remove_menu_page( 'tools.php' );    
        remove_menu_page( 'options-general.php' ); 
   }
        remove_menu_page( 'edit.php' );           
        remove_menu_page( 'edit-comments.php' );           
}
add_action( 'admin_menu', 'remove_menu', 999 );







?>