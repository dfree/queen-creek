<!doctype html>
<html class="no-js" lang="" ng-app="queenCreek">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <?php wp_head(); ?>

        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/angular-embedplayer.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/min/app-min.js"></script>
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/app-content.js"></script>

        <link href='http://fonts.googleapis.com/css?family=Kameron|Roboto:400,300' rel='stylesheet' type='text/css'>

