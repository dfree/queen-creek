<section class="interior">

  <a href="/"><div class="home-page"><&nbsp;&nbsp;Home Page</div></a>

  <div class="switcher">
    <h1>Queen Creek Development Districts</h1>
    <ul>
      <li ng-repeat="location in projects.project" ng-click="inttab.setTab(location.slot)" ng-class="{active: inttab.isSet(location.slot)}" class="tabber">{{location.slot}}</li>
    </ul>
  </div>

  <span ng-repeat="location in projects.project" ng-show="inttab.isSet(location.slot);">

  <div class="cycle-slideshow gallery gallery-{{location.slot}}"
                    data-cycle-fx="scrollHorz" 
                    data-cycle-swipe="true"
                    data-cycle-swipe-fx="scrollHorz"
                    data-cycle-timeout="0"
                    data-cycle-prev=".prev-{{location.slot}}"
                    data-cycle-next=".next-{{location.slot}}"
                    data-cycle-pager=".pager-dots-{{location.slot}}"
                    data-cycle-pager-template="<div class='dot'></div>"
                    data-cycle-log="false"
                    data-cycle-slides="> div">
    <div class="galWrap" ng-repeat="image in location.gallery">
      <img ng-if="image.type=='image'" ng-src="{{image.link}}">
      <embed-video ng-if="image.type=='video'" iframe-id="vimeo{{location.slot}}" api="1" player_id="vimeo{{location.slot}}" ng-href="https://vimeo.com/{{image.link}}"></embed-video>
    </div>
  </div>
  <div class="pager">
    <div class="prev prev-{{location.slot}}"></div>
    <div class="pager-dots pager-dots-{{location.slot}}"></div>
    <div class="next next-{{location.slot}}"></div>
  </div>

  <div class="inset">
     <img src="{{location.insetImage}}">
  </div>

  
  <div class="location-info">
    
    <div class="number">
      <div class="id"><p>{{location.slot}}</p></div>
      <div class="dots"><img src="<?php echo get_template_directory_uri(); ?>/img/info-box-dots.svg"></div>
    </div>

    <div class="description">
      <h1>{{location.name}}</h1>
      <p>{{location.description}}</p>
    </div>

    <div class="stats">
      <h4>LOCATION //</h4>
      <p>{{location.location}}</p>
      <div class="half">
        <h4>desired use //</h4>
        <p>{{location.use}}</p>
        <span>{{location.subUse}}</span>
      </div>
      <div class="half">
        <h4>available land //</h4>
        <p>{{location.availableLand}}</p>
      </div>

      <h4>downloads //</h4>
      <span ng-repeat="pdf in location.downloads">
      <a href="{{pdf.path}}" target="_blank"><div class="pdf"><img src="<?php echo get_template_directory_uri(); ?>/img/location-pdf-icon.jpg">{{pdf.name}}</div></a>
      </span>
    </div>

    <div class="close">Close</div>

  </div>

  </span>

  <footer><?php include('footer.php') ?></footer>
</section>

