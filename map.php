<section class="map" ng-controller="TabController as tab">
    
    <img src="<?php echo get_template_directory_uri(); ?>/img/background.jpg">

    <div ng-repeat="location in projects.project" class="pin pin-{{location.slot}}">
      <span ng-click="tab.setTab(location.slot)">
        <p ng-class="{zoomP: tab.isSet(location.slot)}">{{location.slot}}</p>
        <div class="pinImage zoomDown" ng-class="{zoomUp: tab.isSet(location.slot)}"></div>
      </span>
    </div>
    
    <div class="road rittenhouse"><p>Rittenhouse</p><img src="<?php echo get_template_directory_uri(); ?>/img/road-rittenhouse.svg"></div>
    <div class="road ocotillo"><p>Ocotillo</p><img src="<?php echo get_template_directory_uri(); ?>/img/road-ocotillo.svg"></div>
    <div class="road ellsworth"><p>Ellsworth</p><img src="<?php echo get_template_directory_uri(); ?>/img/road-ellsworth.svg"></div>
    <div class="road ellsworth-loop"><p>Ellsworth Loop</p><img src="<?php echo get_template_directory_uri(); ?>/img/road-ellsworth-loop.svg"></div>

    <div class="info-box">
      <div class="info" >
        
        <span ng-repeat="location in projects.project" ng-show="tab.isSet(location.slot);">
        <div class="image"><img src="{{location.homeImage}}"></div>
        
        <div class="text">
          <h1>{{location.name}}</h1>
          <h4>LOCATION //</h4>
          <p>{{location.location}}</p>
          <div class="half">
          <h4>desired use //</h4>
          <p>{{location.use}}</p>
          <span>{{location.subUse}}</span>
        </div>
        <div class="half">
          <h4>available land //</h4>
          <p>{{location.availableLand}}</p>
        </div>
          <div class="learn-more loc" ng-click="inttab.setTab(location.slot)">LEARN MORE ></div>
        </div>
        </span>
        
        <div class="number">
          <div class="id"><p>{{tab.tab}}</p></div>
          <div class="dots"><img src="<?php echo get_template_directory_uri(); ?>/img/info-box-dots.svg"></div>
        </div>
      </div>
      <div class="info-nav">
        <p><span>CLICK TO Explore</span> all THE Queen creek development districts</p>
        <ul>
          <li>|</li>
          <li ng-repeat="location in projects.project" ng-click="tab.setTab(location.slot)" ng-class="{active: tab.isSet(location.slot)}">{{location.slot}}</li>
        </ul>
      </div>
    </div>

</section>


<section class="mobile-home">

  <img src="<?php echo get_template_directory_uri(); ?>/img/background.jpg">

  <div class="mobile-inset">
    <p>Explore all of our<br />development districts</p>
    <p class="arrows">below</p>
    <img src="<?php echo get_template_directory_uri(); ?>/img/mobile-home-inset.svg">
  </div>

  <ul class="project-list">
    <li class="loc" ng-repeat="location in projects.project" ng-click="inttab.setTab(location.slot)">
      <p>{{location.slot}}</p>
      <h1>{{location.name}}</h1>
    </li>
  </ul>

  <div class="mobilefooter">
    <div class="social">
      <ul>
        <li class="twitter"></li>
        <li class="facebook"></li>
      </ul>
    </div>

    <p>Town of Queen Creek, Economic Development<br />
      22358 S Ellsworth Road Queen Creek, AZ  85142<br />
      480 358 3523  |  investtheqc@queencreek.org</p>

    <img src="<?php echo get_template_directory_uri(); ?>/img/footer-invest.svg">
  </div>

</section>

<footer class="homefooter"><?php include('footer.php') ?></footer>


