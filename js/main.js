/*

  Queen Creek Development District

  Site Development by Kitchen Sink Studios, Inc
  Developer: Daniel Freeman [@dFree]

*/

$(document).ready(function(){
$('body').transition({opacity: 1}, 1000, 'ease');
});//end



$(window).load(function(){
// GLOBALS -----------------------
var windowWidth = $(window).width(),
    windowHeight = $(window).height();

// BROWER DETECTION -----------------------
//mobile detection
var device = navigator.userAgent.toLowerCase();
var mobileos = device.match(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i);

//desktop detection
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) !== -1) {
                return data[i].identity;
            }
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index === -1) {
            return;
        }

        var rv = dataString.indexOf("rv:");
        if (this.versionSearchString === "Trident" && rv !== -1) {
            return parseFloat(dataString.substring(rv + 3));
        } else {
            return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
        }
    },
    dataBrowser: [
        {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
        {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
        {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
        {string: navigator.userAgent, subString: "Safari", identity: "Safari"},
        {string: navigator.userAgent, subString: "Opera", identity: "Opera"}
    ]
};
BrowserDetect.init();

var thisBrowser = BrowserDetect.browser;




// WINDOW RESIZE FUNCTIONS -----------------------
$(window).resize(function() {
  windowWidth = $(window).width();
  windowHeight = $(window).height();
});




// SETS DISPLAY OF INSET ON CYCLE CHANGE -----------------------
$('.cycle-slideshow').on('cycle-after', function( event, opts ) {
    var number = $(this).data("cycle.opts").currSlide; 
    if(number == 0){
        $('.inset').transition({opacity: 1.0, x: '0px'}, 500, 'ease');
    }
    var checkVid = opts.slides[number].innerHTML;
    var isVid = checkVid.indexOf('iframe');
    if(isVid == '123'){
      var vidID = ('#' + $(this).find('iframe').attr('id') );
      var iframe = $(vidID)[0];
      var player = $f(iframe);
      player.api('play');
    }
});
$('.cycle-slideshow').on('cycle-before', function( event, opts ) {
    var number = $(this).data("cycle.opts").currSlide; 
    pauseAll(); 
    if(number == 0){
        $('.inset').transition({opacity: 0.0, x: '-150px'}, 800, 'ease');
    }
});


//checks slide number on tab switch
$(".tabber").click(function(){
    pauseAll();
    var tabNum = $(this).html();
    var number = $('.gallery-' + tabNum).data("cycle.opts").currSlide; 
    console.log('tabber:' + number);
    if(number == 0){
        $('.inset').transition({opacity: 1.0, x: '0px'}, 500, 'ease');
    }else{
        $('.inset').transition({opacity: 0.0, x: '-150px'}, 800, 'ease');
    }
});


// GALLERY PAGER WIDTH -----------------------
$('.pager').each(function(){
    var dotCount = $(this).find('.pager-dots').children('div').length;
    var size = (45+(dotCount*35));
    $(this).css({width: size});
});


$('.loc').click(function(event){
    event.preventDefault();
    var intHeight = ($('.interior').height())+112;
    //set height if video iframes
    if (windowWidth < 1100){
        var galWrapWidth = windowWidth;
    } else {
        var galWrapWidth = 1100;
    }
    var iframeHeight = (galWrapWidth * 9/16)-30;
    $('.galWrap iframe').css({height: iframeHeight});
    //create transitions for desktop and mobile
    if (windowWidth > 1000){
        $('.map').transition({y: '-150%'}, 800, 'ease');
    } else {
        $('.mobile-home').transition({x: '-100%'}, 400, 'ease', function(){
            $("html, body").animate({ scrollTop: 0 }, 'fast');
        });
    }
    $('.homefooter').transition({y: '350%', opacity: 0}, 1000, 'ease', function(){
        $(this).css({display: 'none'});
    });
    $('body').transition({height: intHeight}, 300, 'linear');
});

var origBodyHeight = $('body').height();
$('.home-page, .close').click(function(event){
    event.preventDefault();
    pauseAll();
    if (windowWidth > 1000){
        $('.map').transition({y: '0%'}, 400, 'ease');
    } else {
        $('.mobile-home').transition({x: '0%'}, 400, 'ease', function(){
            $("html, body").animate({ scrollTop: 0 }, 'fast');
        });
    }

    $('.homefooter').css({display: 'block'}).transition({y: '0%', opacity: 1}, 400, 'ease');
    $('body').transition({height: origBodyHeight}, 300, 'linear');
});




// Vimeo pause on play
  var iframe;
  var player;

  //loop through all players and add events
  $('iframe').each(function(){
    // var playerName = ('#vimeo1');
    var playerName = ('#' + $(this).attr('id'));
    console.log('video:' + playerName);

    iframe = $(playerName)[0];
    player = $f(iframe);
    // When the player is ready, add listeners
    player.addEvent('ready', function() {
      player.addEvent('play', onPlay);
      player.addEvent('pause', onPause);
      player.addEvent('finish', onFinish);
    });
  });

  //play, pause, finish functions
  function onPlay(id){console.log('play');}
  function onPause(id){console.log('pause');}
  function onFinish(id){console.log('finish');}


  function pauseAll(){
    $('iframe[src*="vimeo.com"]').each(function () {
        $f(this).api('pause');
    });
  }


});//end