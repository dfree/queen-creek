/*

  Queen Creek

  Site Development by Kitchen Sink Studios, Inc
  Developer: Daniel Freeman [@dFree]

*/
(function() {

var app = angular.module('queenCreek', ['videosharing-embed']);

//PROJECT CONTROLLER
app.controller('ProjectController', function(){
    this.project = developments;
});



// TAB CONTROLLER
app.controller('TabController', function(){
    this.tab = 1;

    this.setTab = function(newValue){
      this.tab = newValue;
    };

    this.isSet = function(tabName){
      return this.tab === tabName;
    };

    this.getTab = function(){
      return this.tab;
    };

});




})();
